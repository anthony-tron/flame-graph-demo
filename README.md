# Flame Graph Demo (optimizing in React)

This repository is based on [bvaughn's flame graph demo repository](https://github.com/bvaughn/live-stream-deep-dive-react-profiler) but with - hopefully - more modern syntax.

The aim is to highlight three ways to improve performance in React:

1. Virtualized Lists

Virtualized lists are used for displaying a large amount of data. They achieve this by only inserting the elements in
the DOM that are in a visible area.

This example uses `react-virtualized-auto-sizer` and `react-window`.

Example:

```jsx
// [0, 1, 2, ..., 1999]
const items = Array(2000).fill(0).map((_, i) => i);

const Example = () => {
  // Autosizer gives the dimensions of the parent element
  return (
    <AutoSizer>
      {({ width, height }) => (
        <List width={width} height={height} items={items} />
      )}
    </AutoSizer>
  );
};

const List = ({items, width, height}) => {
  return (
    <FixedSizeList height={height} width={width} itemCount={items.length} itemSize={30} itemData={{items}}>
      {ListItem}
    </FixedSizeList>
  );
};

const ListItem = ({index, style, data: {items}}) => (
  <li style={style}>
    {items[index]}
  </li>
);
```


2. Memoizing
 
Memoizing is a way to remember a value such as the return value of an expensive function to avoid executing it when its
result is predictable (ex: if the function has already been called with the same arguments, return the cached value
instead of calling the function).

This example uses `memoize-one` which stores a single cached value for a memoized function. This means the cached value
is used if the function is called with the same arguments of the previous call.

Example:
```js
const factorial = (i) => {
  if (i === 0)
    return 1;
  return i * factorial(i - 1);
};

const memoizedFactorial = memoizeOne(factorial);

memoizedFactorial(20); // called factorial

memoizedFactorial(25); // called factorial

memoizedFactorial(20); // called factorial
memoizedFactorial(20); // returned cached value

memoizedFactorial(25); // called factorial
memoizedFactorial(25); // returned cached value
```

 
3. Avoiding inner components

Declaring a component inside another component forces React to mount the inner component every render.

Example 1a:

```jsx
const Hello = ({ name }) => {
  // Peformance could be improved
  const InnerGreeting = () => <span>deer {name}</span>;
  
  return (
    <div>
      Hello, <InnerComponent />
    </div>
  );
};
```

Example 1b:
```jsx
// Moved outside of Hello
const Greeting = ({ name }) => <span>deer {name}</span>;
  
const Hello = ({ name }) => {
  // No inner components
  return (
    <div>
      Hello, <InnerComponent />
    </div>
  );
};
```

This goes the same for render components.

Example 2a:
```jsx
const Hello = ({ name, children }) => {
  return (
    <div>
      {children(`Hello, ${name}`)}
    </div>
  );
};

// Using an inline render component
const App = () => (
  <Hello>
    {(greet) => (
      <span>
        {greet}
      </span>
    )}
  </Hello>
);
```


Example 2a:
```jsx
const Hello = ({ name, children }) => {
  return (
    <div>
      {children({ greet: `Hello, ${name}`})}
    </div>
  );
};

// Render component has its own function
const Greeting = ({ greet }) => (
  <span>
    {greet}
  </span>
);

const App = () => (
  <Hello>
    {Greeting}
  </Hello>
);
```
