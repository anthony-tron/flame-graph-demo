import testData from './assets/live.json';
import FlameChart from './FlameChart';
import AutoSizer from 'react-virtualized-auto-sizer';

import './App.css';
import { useState } from 'react';

const App = () => {
  const [data, setData] = useState(null);

  return (
    <>
      <button onClick={() => setData(testData)}>Load</button>
      {data && (
        <AutoSizer>
          {({ height, width }) => (
            <FlameChart data={data} width={width} height={height} />
          )}
        </AutoSizer>
      )}
    </>
  );
};

export default App
