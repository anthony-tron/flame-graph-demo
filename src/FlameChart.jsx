import { convert } from './utils';

import { FixedSizeList } from 'react-window';
import memoize from 'memoize-one';

const memoizedConvert = memoize(convert);
const getItemData = memoize((listData, width) => ({ listData, width }));

const FlameChart = ({ data, height, width }) => {
  const listData = memoizedConvert(data);
  const itemData = getItemData(listData, width);

  return (
    <FixedSizeList height={height} width={width} itemCount={listData.length} itemSize={30} itemData={itemData}>
      {ListItem}
    </FixedSizeList>
  );
};

const ListItem = ({ index, style, data }) => (
  <div style={style}>
    {data.listData[index].map((node, i) => (
      <div
        key={i}
        className="Node"
        style={{
          left: node.offset * data.width,
          width: node.width * data.width,
          backgroundColor: node.color,
        }}
      >
        {node.name}
      </div>
    ))}
  </div>
);

export default FlameChart;
